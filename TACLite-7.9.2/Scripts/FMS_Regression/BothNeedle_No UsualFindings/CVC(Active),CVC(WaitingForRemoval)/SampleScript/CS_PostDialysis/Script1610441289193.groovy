import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.Keys as Keys
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties  staticvalues = helperUtilities.getobject('C:\\katalon\\TACLite\\Data Resources\\CS_EnvVariables.properties')

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\PostDialysis.properties')

Properties prop1 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\EnvironmentalVariables.properties')

TestObject testObj=new TestObject()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('BackToCS'), '', 'scrolltoelement')
WebUI.delay(1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('BackToCS'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PostDialysis'),'','click')
WebUI.delay(3)
WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'),'','switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PostDialysis_Update1'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')


Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//ReadWrite.xlsx', 'Data', true)
def PreWeight=excelData.getValue('PreWeight', 1)
println(PreWeight)


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PostWeight'),''+PreWeight,'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_SitBP'),staticvalues.getProperty("Post_SitBP"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_DiastolicBP'),staticvalues.getProperty("Post_DiastolicBP"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_Resp'),staticvalues.getProperty("Post_Resp"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_O2'),staticvalues.getProperty("Post_O2"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_Pulse'),staticvalues.getProperty("Post_Pulse"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_IR'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_AvgBFR'),staticvalues.getProperty("Post_AvgBFR"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_AvgDFR'),staticvalues.getProperty("Post_AvgDFR"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_SysStandingBP'),staticvalues.getProperty("Post_SysStandingBP"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_DiasStandingBP'),staticvalues.getProperty("Post_DiasStandingBP"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Temp'),staticvalues.getProperty("Temp"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Interrupt'),staticvalues.getProperty("Interrupt"),'input')
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('OLC'),staticvalues.getProperty("OLC"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('BloodVolume'),staticvalues.getProperty("BloodVolume"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('MeanKECN'),staticvalues.getProperty("MeanKECN"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('VenousPressure'),staticvalues.getProperty("VenousPressure"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Dynamic'),'','click')


DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
LocalDateTime datetime = LocalDateTime.now()
println (''+datetime)
String timeformat=datetime.format(formatter);
String[] postDialTime = timeformat.split("\\s+");
String split_Time=postDialTime[1];
println (''+split_Time)
postTime=''+split_Time

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_TimeObserved'),''+postTime,'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'),'','click')
WebUI.delay(5)
CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("Username"),prop1.getProperty("Password"))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_Continue'), '', 'click')
WebUI.takeScreenshot()
//------------------------------------------Update button 2-------------------------------
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'),'','switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PostDialysis_Update2'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_Findings'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('DischargedTo'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreDialysis_qn'),'','scrolltoelement')
WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreDialysis_qn'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Adverseevent_qn'),'','scrolltoelement')
WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Adverseevent_qn'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_TimeObserved'),''+postTime,'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'),'','click')
WebUI.delay(5)
CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("Username"),prop1.getProperty("Password"))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_continue1'), '', 'click')
WebUI.takeScreenshot()
//-------------------------------------------------End Treatment------------------------------------------------
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('EndTreatment'), '', 'scrolltoelement')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('EndTreatment'), '', 'click')

if (prop.getProperty('End_OK')) {
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('End_OK'), '', 'click')
	
}


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('End_Tx_Ok'), '', 'click')

WebUI.delay(5)
CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("Username"),prop1.getProperty("Password"))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PostDialysis_Time'), ''+postTime,'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Postdialysis_Continue'), '', 'click')


def a=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Unexp_Ok'))
println(a)
if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)){
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Unexp_Ok'), '', 'click')
}
else{
	println("No OK button")
}

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('EndSession'), '', 'click')
WebUI.delay(5)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Tx_Expected'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('EndTx_Continue'), '', 'click')
WebUI.delay(5)
WebUI.takeScreenshot('PostDialysis.png')

Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def PatientName=excelData1.getValue('PatientName', 2)
println(PatientName)

String PtCompleted=WebUI.getAttribute(new TestObject().addProperty('xpath',ConditionType.EQUALS, prop.getProperty('TreatmentStatus')+PatientName+prop.getProperty('TreatmentStatus1')),'value')
println(''+PtCompleted)

