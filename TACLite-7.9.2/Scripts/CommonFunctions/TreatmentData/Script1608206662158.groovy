import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.Keys as Keys
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\TreatmentData.properties')

Properties prop1 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\EnvironmentalVariables.properties')

Properties  staticvalues = helperUtilities.getobject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\CS_EnvVariables.properties')
/*
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('BackToCS'), '', 'scrolltoelement')
WebUI.delay(1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('BackToCS'), '', 'click')
*/
CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Treatment_Data'),'','click')
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Treatment'), '', 'switchframe')


WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Add_Manual'),'','click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Time'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Hemaclip'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Effluent'),staticvalues.getProperty("Effluent"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FF'),staticvalues.getProperty("FF"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Dialysate'),staticvalues.getProperty("DialysateVol"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('UF'),staticvalues.getProperty("UF"),'input')
WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'),'','click')
WebUI.delay(2)
CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("Username"),prop1.getProperty("Password"))
WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Continue'), '', 'click')

WebUI.delay(2)


WebUI.takeScreenshot()




