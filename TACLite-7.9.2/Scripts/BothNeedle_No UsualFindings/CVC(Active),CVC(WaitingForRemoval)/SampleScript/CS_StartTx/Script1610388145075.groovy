import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import reusablekeyword.Reusable as Reusable

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

Properties prop2 = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\FluidManagement.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop2.getProperty('BackToCS'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('StartTx'), '', 'scrolltoelement')
WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('StartTx'), '', 'click')

WebUI.delay(2)
TestObject testObj = new TestObject()

def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Start_continue'))
WebUI.delay(3)
if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)) {
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
	
	CustomKeywords.'reusablekeyword.Reusable.TxStart'()
	
	Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C:\\katalon\\TACLite\\InputData\\ReadWrite.xlsx', 'Data1',
		true)
	WebUI.takeScreenshot()
	def Time = excelData1.getValue('Time', 1)
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Starttx_time'), '' + Time, 'input')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Start_continue'), '', 'click')
	
   
} else {
    WebUI.callTestCase(findTestCase('CommonFunctions/FluidManagement'), [:], FailureHandling.STOP_ON_FAILURE)

	CustomKeywords.'reusablekeyword.Reusable.TxStart'()
	
	Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C:\\katalon\\TACLite\\InputData\\ReadWrite.xlsx', 'Data1',
		true)
	
	def Time = excelData1.getValue('Time', 1)
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Starttx_time'), '' + Time, 'input')
	WebUI.takeScreenshot()
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Start_continue'), '', 'click')
	
	//CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')
	CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')
	WebUI.delay(2)
	CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreTxButton'), '', 'click')
	
	}


WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Access_Eval'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')



