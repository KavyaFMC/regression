import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.annotation.Keyword as Keyword
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.testdata.InternalData as InternalData
import java.io.FileInputStream as FileInputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.FileOutputStream as FileOutputStream
import java.io.IOException as IOException
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import java.sql.Driver as Driver
import java.util.concurrent.TimeUnit as TimeUnit
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'scrolltoelement')

WebUI.delay(3)


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'click')

WebUI.delay(3)

TestObject testObj = new TestObject()

def a = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Closebtn'))
WebUI.comment("Check for appointment reminders and click on close button")

if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)) {
    CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Closebtn'), '', 'click')
} else {
    println('Proceed to next-do nothing')
}



CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select1'), '', 'click')

WebUI.comment("Check if post-surgical assessment is present")

WebUI.callTestCase(findTestCase('CommonFunctions/PostSurgical'), [:], FailureHandling.OPTIONAL)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', 
    true)

def PreTx_Look1 = excelData.getValue('PreTx_Look1', 1)

println(PreTx_Look1)

CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Look1') + PreTx_Look1) + prop.getProperty(
        'Pre'), '', 'click')

def PreTx_Listen1 = excelData.getValue('PreTx_Listen1', 1)

println(PreTx_Listen1)

CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Listen1') + PreTx_Listen1) + prop.getProperty(
        'Pre'), '', 'click')

WebUI.delay(3)
WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)

def PreTx_Feel1 = excelData.getValue('PreTx_Feel1', 1)

println(PreTx_Feel1)

CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Feel1') + PreTx_Feel1) + prop.getProperty(
        'Pre'), '', 'click')

def PreTx_Other1 = excelData.getValue('PreTx_Other1', 1)

println(PreTx_Other1)

CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Other1') + PreTx_Other1) + prop.getProperty(
        'Pre'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select2'), '', 'click')

WebUI.callTestCase(findTestCase('CommonFunctions/PostSurgical'), [:], FailureHandling.OPTIONAL)

def PreTx_Look2 = excelData.getValue('PreTx_Look2', 1)

println(PreTx_Look2)

CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Look1') + PreTx_Look2) + prop.getProperty(
        'Pre'), '', 'click')
WebUI.delay(3)
def PreTx_Listen2 = excelData.getValue('PreTx_Listen2', 1)

println(PreTx_Listen2)

CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Listen1') + PreTx_Listen2) + prop.getProperty(
        'Pre'), '', 'click')

def PreTx_Feel2 = excelData.getValue('PreTx_Feel2', 1)

println(PreTx_Feel2)

CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Feel1') + PreTx_Feel2) + prop.getProperty(
        'Pre'), '', 'click')
WebUI.takeScreenshot()
def PreTx_Other2 = excelData.getValue('PreTx_Other2', 1)

println(PreTx_Other2)

CustomKeywords.'pageLocators.pageOperation.pageAction'((prop.getProperty('Pre_Other1') + PreTx_Other2) + prop.getProperty(
        'Pre'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()



