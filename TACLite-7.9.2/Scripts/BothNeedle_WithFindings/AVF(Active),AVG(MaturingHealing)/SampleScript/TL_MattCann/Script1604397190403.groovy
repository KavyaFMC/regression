import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.IOException
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//TacLite_Properties//MaturationCannulation.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

Properties prop2 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//DynamicProperties.properties')

TestObject testObj=new TestObject()

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Maturation'), '', 'scrolltoelement')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Maturation'), '', 'click')

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.comment("Check if the Access is in Ready to Cannulate state, Already Cannulated state or no")

def check=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Checkmark'))

def ready=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Ready'))

def alreadycann=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('AlreadyCann'))

def btndisabled=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('ButtonDisabled'))

if (WebUI.waitForElementPresent(check, 3, FailureHandling.OPTIONAL)){
	
if (WebUI.waitForElementPresent(ready, 3, FailureHandling.OPTIONAL)){
	
	println("Access is in Ready to Cannulate state")
}
else{
	println("No-Ready")
}
}

if (WebUI.waitForElementPresent(check, 3, FailureHandling.OPTIONAL)){
	
if (WebUI.waitForElementPresent(alreadycann, 3, FailureHandling.OPTIONAL)){
	println("Access is in Already Cannulated state")
}
else{
	println("No-Already")
}
}

if (WebUI.waitForElementPresent(btndisabled, 3, FailureHandling.OPTIONAL)){
	println("Button is in disabled state")
}

else{
	
	println("Cannulation to be done")

WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('select1'), '', 'click')
WebUI.delay(3)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_Findings', true)
def Cann_Look=excelData.getValue('Cann_Look', 4)
println(Cann_Look)
def Cann_Look_Value=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Cann_look')+Cann_Look+prop.getProperty('AVG_Cann'))

if (WebUI.waitForElementPresent(Cann_Look_Value, 3, FailureHandling.OPTIONAL)){
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_look')+Cann_Look+prop.getProperty('AVG_Cann'), '', 'click')
}

def Cann_Listen=excelData.getValue('Cann_Listen', 4)
println(Cann_Listen)
def Cann_Listen_Value=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Cann_listen')+Cann_Listen+prop.getProperty('AVG_Cann'))

if (WebUI.waitForElementPresent(Cann_Listen_Value, 3, FailureHandling.OPTIONAL)){
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_listen')+Cann_Listen+prop.getProperty('AVG_Cann'), '', 'click')
}

def Cann_Feel1=excelData.getValue('Cann_Feel1', 4)
println(Cann_Feel1)
def Cann_Feel1_Value=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Cann_feel')+Cann_Feel1+prop.getProperty('AVG_Cann'))

if (WebUI.waitForElementPresent(Cann_Feel1_Value, 3, FailureHandling.OPTIONAL)){
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Cann_Feel1+prop.getProperty('AVG_Cann'), '', 'click')
}


def Cann_Feel2=excelData.getValue('Cann_Feel2', 4)
println(Cann_Feel2)
def Cann_Feel2_Value=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Cann_feel')+Cann_Feel2+prop.getProperty('AVG_Cann'))

if (WebUI.waitForElementPresent(Cann_Feel2_Value, 3, FailureHandling.OPTIONAL)){
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Cann_Feel2+prop.getProperty('AVG_Cann'), '', 'click')
}

def Cann_Feel3=excelData.getValue('Cann_Feel3', 4)
println(Cann_Feel3)
def Cann_Feel3_Value=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Cann_feel')+Cann_Feel3+prop.getProperty('AVG_Cann'))

if (WebUI.waitForElementPresent(Cann_Feel3_Value, 3, FailureHandling.OPTIONAL)){
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Cann_Feel3+prop.getProperty('AVG_Cann'), '', 'click')
}

def Cann_Feel4=excelData.getValue('Cann_Feel4', 4)
println(Cann_Feel4)

def Cann_Feel4_Value=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Cann_feel')+Cann_Feel4+prop.getProperty('AVG_Cann'))

if (WebUI.waitForElementPresent(Cann_Feel4_Value, 3, FailureHandling.OPTIONAL)){
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_feel')+Cann_Feel4+prop.getProperty('AVG_Cann'), '', 'click')
}
WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(3)



}








