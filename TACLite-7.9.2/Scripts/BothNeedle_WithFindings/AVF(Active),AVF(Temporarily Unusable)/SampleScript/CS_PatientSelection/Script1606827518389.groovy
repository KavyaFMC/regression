import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testdata.InternalData
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import java.sql.Driver
import java.util.concurrent.TimeUnit

import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import com.kms.katalon.core.testobject.ConditionType

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//ChairSide_Properties//PatientSelection.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

TestObject testObj = new TestObject()

WebUI.comment('Launch url and login to the application')

//to configure firefox path incase firefox doesnot open

System.setProperty("webdriver.firefox.bin","C:\\Automation\\Mozilla Firefox\\firefox.exe");

WebUI.openBrowser('')

//WebUI.navigateToUrl(prop1.getProperty('QAURL'))

WebUI.comment('Profile for URL selection')
WebUI.navigateToUrl(GlobalVariable.URL)

WebUI.maximizeWindow()

WebUI.waitForPageLoad(5)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Username'), prop1.getProperty('Username'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Password'), prop1.getProperty('Password'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Submit'), '', 'click')

WebUI.comment('Check if login is successful')

def a=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Clinic_landing'))

if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)){
	WebUI.comment("Login to ChairSide application is successful")
}
else{
	WebUI.closeBrowser()
}

WebUI.comment('Fetch the required values from data sheet')
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_Findings', true)
def Clinic=excelData.getValue('Clinic', 1)

def Shift=excelData.getValue("Shift", 1)

def PatientName=excelData.getValue("PatientName", 1)

WebUI.comment('Clinic,shift and patient selection')

CustomKeywords.'pageLocators.pageOperation.pageAction'(((prop.getProperty('Clinic_selection') + Clinic + ' - ') + prop.getProperty(
	'Single_quote')) + prop.getProperty('Single_braces'), '', 'click')


WebUI.waitForPageLoad(5)
WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Shift')+prop.getProperty('Single_quote')+Shift+' - '+prop.getProperty('Single_quote')+prop.getProperty('shift1'), '','click')
WebUI.delay(2)

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Shift')+prop.getProperty('Single_quote')+Shift+' - '+prop.getProperty('Single_quote')+prop.getProperty('shift1'), '','click')


WebUI.takeScreenshot()


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PtSelect')+prop.getProperty('Single_quote')+PatientName+prop.getProperty('Single_quote')+prop.getProperty('PtSelect1'), '', 'click')

WebUI.delay(2)

WebUI.comment("Check if button is present")
def b=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Button_Ok'))

if (WebUI.waitForElementPresent(b, 3, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}


WebUI.comment("Check if button is present")
def d=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Button_right'))

if (WebUI.waitForElementPresent(d, 3, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Button_right'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Update'), '', 'switchframe')

WebUI.delay(2)

WebUI.comment("Check if button is present")
def e=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Button_Ok'))

if (WebUI.waitForElementPresent(e, 3, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Button_Ok'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}


