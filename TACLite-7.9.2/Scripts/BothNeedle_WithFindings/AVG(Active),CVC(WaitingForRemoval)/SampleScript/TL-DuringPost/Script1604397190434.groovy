
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.IOException

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities

import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\During,Post.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_Post'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_Post'), '', 'click')

WebUI.delay(5)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_Select1'), '', 'click')
WebUI.delay(3)

Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_Findings', true)
def During_findings1=excelData1.getValue('During_findings1', 2)
println(During_findings1)

String[] arr = During_findings1.split(",")
for(String x:arr){
	println(''+x)
}

String a1=arr[0]
String b1=arr[1]
String c1=arr[2]
String k1=arr[3]
String l1=arr[4]
String m1=arr[5]
String n1=arr[6]
String o1=arr[7]
String p1=arr[8]
String q1=arr[9]
String r1=arr[10]

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+a1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+a1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+b1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+b1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+c1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+c1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+k1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+k1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+l1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+l1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+m1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+m1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+n1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+n1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+o1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+o1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+p1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+p1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+q1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+q1+prop.getProperty('Post'))
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+r1+prop.getProperty('Post'), '', 'click')
println(prop.getProperty('During_findings')+r1+prop.getProperty('Post'))
WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_Select1'), '', 'click')
WebUI.delay(7)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_tab'), '', 'click')
WebUI.delay(3)

def Post_findings=excelData1.getValue('Post_findings1', 2)
println(Post_findings)


String[] arr1 = Post_findings.split(",")
for(String x:arr1){
	println(''+x)
}

String a2=arr1[0]
String b2=arr1[1]
String c2=arr1[2]
String d2=arr1[3]

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_findings')+a2+prop.getProperty('Post'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_findings')+b2+prop.getProperty('Post'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_findings')+c2+prop.getProperty('Post'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_findings')+d2+prop.getProperty('Post'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(3)




















