import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.Keys as Keys


WebUI.comment('Load the required property files')
Properties propNx2 = helperUtilities.getNxProp3('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_MachineSetup.properties')
Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//MachineSetup.properties')
Properties propNx = helperUtilities.getNxProp1('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_PropertyValues.properties')
Properties propNx1 = helperUtilities.getNxProp2('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_EnvVariables.properties')

WebUI.comment('Machine Setup tab click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('MachineSetup_Update'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Frame_Button'), '', 'defaultcontent')

WebUI.delay(3)

WebUI.comment('ENter values in teh Machine Setup tab fields')
CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('CyclerSerialInput'), propNx.getProperty('CyclerSerialInput'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('StationInput'), propNx.getProperty('StationInput'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('AlarmtestCheck'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('MachineTypeButton'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('NewBatchValue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('PureFlowButton'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('PFSLSerialInput'), propNx.getProperty('PFSLSerialInput'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('PAKLotInput'), propNx.getProperty('PAKLotInput'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('SAKLotInput'), propNx.getProperty('SAKLotInput'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('PerformedBy'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('Performed_User'), propNx1.getProperty('Username'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('Performed_Sign'), propNx1.getProperty('Password'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('Performed_Continue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Frame_Update'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Button_Ok'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('MachineSetup_Update'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Frame_Button'), '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('ReviewedBy'), '', 'scrolltoelement')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('ReviewedBy'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('Performed_User'), propNx1.getProperty('ReviewedUser'), 'input')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('Performed_Sign'), propNx1.getProperty('Password'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx2.getProperty('Performed_Continue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Frame_Update'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Button_Ok'), '', 'click')

WebUI.takeScreenshot()



