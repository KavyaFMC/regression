
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import com.kms.katalon.core.testobject.ConditionType as ConditionType

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'scrolltoelement')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'click')
WebUI.delay(3)

TestObject testObj = new TestObject()

def post = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Closebtn'))
WebUI.comment("Check for appointment reminders and click on close button")

if (WebUI.waitForElementPresent(post, 3, FailureHandling.OPTIONAL)) {
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Closebtn'), '', 'click')
} else {
	println('Proceed to next-do nothing')
}


WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select1'), '', 'click')

WebUI.comment("Check if post-surgical assessment is present")

WebUI.callTestCase(findTestCase('CommonFunctions/PostSurgical'), [:], FailureHandling.STOP_ON_FAILURE)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_Findings', true)
def PreTx_Look1=excelData.getValue('PreTx_Look1', 4)
println(PreTx_Look1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look1+prop.getProperty('Pre'), '', 'click')

def PreTx_Listen1=excelData.getValue('PreTx_Listen1', 4)
println(PreTx_Listen1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen1+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)

def PreTx_Feel1=excelData.getValue('PreTx_Feel1', 4)
println(PreTx_Feel1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel1+prop.getProperty('Pre'), '', 'click')

WebUI.takeScreenshot()

def PreTx_Other1=excelData.getValue('PreTx_Other1', 4)
println(PreTx_Other1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other1+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select2'), '', 'click')

WebUI.comment("Check if post-surgical assessment is present")

WebUI.callTestCase(findTestCase('CommonFunctions/PostSurgical'), [:], FailureHandling.STOP_ON_FAILURE)

def PreTx_Look2=excelData.getValue('PreTx_Look2', 4)
println(PreTx_Look2)

WebUI.delay(3)

String[] arr = PreTx_Look2.split(",")
for(String x:arr){
	println(''+x)
}
String a=arr[0]
String b=arr[1]
String c=arr[2]
String k=arr[3]
String l=arr[4]
String m=arr[5]
String n=arr[6]
String o=arr[7]

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+a+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+b+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+c+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+k+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+l+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+m+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+n+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+o+prop.getProperty('Pre'), '', 'click')

def PreTx_Listen2=excelData.getValue('PreTx_Listen2', 4)
println(PreTx_Listen2)

String[] arr1 = PreTx_Listen2.split(",")
for(String y:arr1){
	println(''+y)
}
String d=arr1[0]
String e=arr1[1]

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+d+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+e+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)

def PreTx_Feel2=excelData.getValue('PreTx_Feel2', 4)
println(PreTx_Feel2)


String[] arr2 = PreTx_Feel2.split(",")
for(String z:arr2){
	println(''+z)
}
String f=arr2[0]
String g=arr2[1]
String h=arr2[2]
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+f+prop.getProperty('Pre'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+g+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+h+prop.getProperty('Pre'), '', 'click')

def PreTx_Other2=excelData.getValue('PreTx_Other2', 4)
println(PreTx_Other2)

String[] arr3 = PreTx_Other2.split(",")
for(String v:arr3){
	println(''+v)
}
String i=arr3[0]
String j=arr3[1]

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+i+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+j+prop.getProperty('Pre'), '', 'click')

WebUI.takeScreenshot()

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(2)



