
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.IOException

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities

import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\During,Post.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_Post'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_Post'), '', 'click')

WebUI.delay(5)

WebUI.takeScreenshot()


WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_Select1'), '', 'click')
WebUI.delay(3)

Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_Findings', true)
def During_findings1=excelData1.getValue('During_findings1', 4)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_findings')+During_findings1+prop.getProperty('Post'), '', 'click')

WebUI.takeScreenshot()

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('During_Select1'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_tab'), '', 'click')
WebUI.delay(3)

def Post_findings=excelData1.getValue('Post_findings1', 4)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Post_findings')+Post_findings+prop.getProperty('Post'), '', 'click')

WebUI.takeScreenshot()

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(3)


























