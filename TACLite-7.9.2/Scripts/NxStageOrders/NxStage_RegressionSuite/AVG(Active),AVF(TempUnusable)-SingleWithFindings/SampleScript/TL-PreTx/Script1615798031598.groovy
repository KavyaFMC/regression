
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'scrolltoelement')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'click')
WebUI.delay(3)

WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select1'), '', 'click')

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_Findings', true)
def PreTx_Look1=excelData.getValue('PreTx_Look1', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look1+prop.getProperty('Pre'), '', 'click')

def PreTx_Listen1=excelData.getValue('PreTx_Listen1', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen1+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

def PreTx_Feel1=excelData.getValue('PreTx_Feel1', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel1+prop.getProperty('Pre'), '', 'click')

def PreTx_Other1=excelData.getValue('PreTx_Other1', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other1+prop.getProperty('Pre'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select2'), '', 'click')
def PreTx_Look2=excelData.getValue('PreTx_Look2', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look2+prop.getProperty('Pre'), '', 'click')

def PreTx_Listen2=excelData.getValue('PreTx_Listen2', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen2+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)
def PreTx_Feel2=excelData.getValue('PreTx_Feel2', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel2+prop.getProperty('Pre'), '', 'click')

def Pre_Other2=excelData.getValue('PreTx_Other2', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+Pre_Other2+prop.getProperty('Pre'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(2)

WebUI.takeScreenshot()
WebUI.delay(3)




