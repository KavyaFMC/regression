import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException as IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import com.kms.katalon.core.annotation.Keyword as Keyword
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.testdata.InternalData as InternalData
import java.io.FileInputStream as FileInputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.FileOutputStream as FileOutputStream
import java.lang.CharSequence as CharSequence
import java.sql.Driver as Driver
import java.util.concurrent.TimeUnit as TimeUnit
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import utilityKeywords.helperUtilities as helperUtilities

WebUI.comment('Load the required property files')
Properties propNx2 = helperUtilities.getNxProp3('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_MachineSetup.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//PatientSelection.properties')

Properties propNx = helperUtilities.getNxProp1('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_PropertyValues.properties')

Properties propNx1 = helperUtilities.getNxProp2('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_EnvVariables.properties')

TestObject testObj = new TestObject()

WebUI.comment('launch url and login to the application')

//to configure firefox path incase firefox doesnot open

System.setProperty("webdriver.firefox.bin","C:\\Automation\\Mozilla Firefox\\firefox.exe");

WebUI.openBrowser('')

WebUI.navigateToUrl(propNx1.getProperty('QAURL'))

WebUI.maximizeWindow()

WebUI.waitForPageLoad(5)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Username'), propNx1.getProperty('Username'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Password'), propNx1.getProperty('Password'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Submit'), '', 'click')

def a=testObj.addProperty('xpath', ConditionType.EQUALS, prop1.getProperty('Clinic_landing'))

if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)){
	WebUI.comment("Login to ChairSide application is successful")
}
else{
	WebUI.closeBrowser()
}


WebUI.comment('fetch the required values from excel')
//Object excelData = ExcelFactory.getExcelDataWithDefaultSheet(propNx1.getProperty('Excelpath'), 'NxStageTestData', true)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_Findings', true)

def Clinic = excelData.getValue('Clinic', 1)

def Shift = excelData.getValue('Shift', 1)

def PatientName = excelData.getValue('PatientName', 1)


WebUI.comment('clinic selection')
println((prop1.getProperty('Clinic_selection') + Clinic + ' - ') + prop1.getProperty('Single_quote')) + prop1.getProperty('Single_braces')

CustomKeywords.'pageLocators.pageOperation.pageAction'(((prop1.getProperty('Clinic_selection') + Clinic + ' - ') + prop1.getProperty(
	'Single_quote')) + prop1.getProperty('Single_braces'), '', 'click')


WebUI.comment('shift selection')
CustomKeywords.'pageLocators.pageOperation.pageAction'(((((prop1.getProperty('Shift') + prop1.getProperty('Single_quote')) + 
    Shift) + ' - ') + prop1.getProperty('Single_quote')) + prop1.getProperty('shift1'), '', 'click')

WebUI.delay(5)

CustomKeywords.'pageLocators.pageOperation.pageAction'(((((prop1.getProperty('Shift') + prop1.getProperty('Single_quote')) + 
    Shift) + ' - ') + prop1.getProperty('Single_quote')) + prop1.getProperty('shift1'), '', 'click')

WebUI.comment('screenshot')
WebUI.takeScreenshot()

WebUI.comment('patient selection')
def ptsel=testObj.addProperty('xpath', ConditionType.EQUALS, (((prop1.getProperty('NxPtSelect') + prop1.getProperty('Single_quote')) + 
PatientName) + prop1.getProperty('Single_quote')) + prop1.getProperty('NxPtSelect1'))

if (WebUI.waitForElementPresent(ptsel, 3, FailureHandling.OPTIONAL)){
	
CustomKeywords.'pageLocators.pageOperation.pageAction'((((prop1.getProperty('NxPtSelect') + prop1.getProperty('Single_quote')) + 
PatientName) + prop1.getProperty('Single_quote')) + prop1.getProperty('NxPtSelect1'), '', 'click')

}

else{
	println("Proceed to next-do nothing")
}
	
	
def ptsel1=testObj.addProperty('xpath', ConditionType.EQUALS, (((prop1.getProperty('NxSelect') + prop1.getProperty('Single_quote')) + 
PatientName) + prop1.getProperty('Single_quote')) + prop1.getProperty('NxSelect1'))

if (WebUI.waitForElementPresent(ptsel1, 3, FailureHandling.OPTIONAL)){

	CustomKeywords.'pageLocators.pageOperation.pageAction'((((prop1.getProperty('NxSelect') + prop1.getProperty('Single_quote')) +
PatientName) + prop1.getProperty('Single_quote')) + prop1.getProperty('NxSelect1'), '', 'click')
		
}
	
else{
	println("Proceed to next-do nothing")
}

WebUI.delay(2)


WebUI.comment("Check if button is present")
def b=testObj.addProperty('xpath', ConditionType.EQUALS, prop1.getProperty('Button_Ok'))

if (WebUI.waitForElementPresent(b, 3, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Button_Ok'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}

WebUI.comment("Check if button is present")
def c=testObj.addProperty('xpath', ConditionType.EQUALS, prop1.getProperty('Button_Ok'))

if (WebUI.waitForElementPresent(c, 3, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Button_Ok'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}


WebUI.comment("Check if button is present")
def d=testObj.addProperty('xpath', ConditionType.EQUALS, prop1.getProperty('Button_right'))

if (WebUI.waitForElementPresent(d, 3, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Button_right'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Frame_Update'), '', 'switchframe')

WebUI.delay(2)

WebUI.comment("Check if button is present")
def e=testObj.addProperty('xpath', ConditionType.EQUALS, prop1.getProperty('Button_Ok'))

if (WebUI.waitForElementPresent(e, 3, FailureHandling.OPTIONAL)){
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop1.getProperty('Button_Ok'), '', 'click')
}
else{
	println("Proceed to next-do nothing")
}




