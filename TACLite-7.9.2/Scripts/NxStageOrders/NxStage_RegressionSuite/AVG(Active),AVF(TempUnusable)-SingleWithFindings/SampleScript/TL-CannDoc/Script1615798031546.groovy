import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.IOException

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities

import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\Cannulation.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CannDoc'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CannDoc'), '', 'click')
WebUI.delay(3)

WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Select1'), '', 'click')
WebUI.delay(3)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_Findings', true)
def CannCleaned1=excelData.getValue('CannCleaned1', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_AccessCleaned')+CannCleaned1+prop.getProperty('Cann'), '', 'click')

def ArterialAttempts=excelData.getValue('ArterialAttempts', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialAttempts1')+ArterialAttempts+prop.getProperty('Cann'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FinalAttempt1'), '', 'click')

WebUI.delay(5)
def ArterialNeedleSize=excelData.getValue('ArterialNeedleSize', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialNeedleSize')+ArterialNeedleSize+prop.getProperty('Cann'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialNeedleSize')+ArterialNeedleSize+prop.getProperty('Cann'), '', 'click')

def Cann_Outcome1=excelData.getValue('Cann_Outcome1', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Successful')+Cann_Outcome1+prop.getProperty('Cann'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Select2'), '', 'click')
WebUI.delay(3)
def CannCleaned2=excelData.getValue('CannCleaned2', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_AccessCleaned')+CannCleaned2+prop.getProperty('Cann'), '', 'click')

def VenousAttempts=excelData.getValue('VenousAttempts', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('VenousAttempts1')+VenousAttempts+prop.getProperty('Cann'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FinalAttempt1'), '', 'click')
def VenousNeedleSize=excelData.getValue('VenousNeedleSize', 1)
WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('VenousNeedleSize')+VenousNeedleSize+prop.getProperty('Cann'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('VenousNeedleSize')+VenousNeedleSize+prop.getProperty('Cann'), '', 'click')
def Cann_Outcome2=excelData.getValue('Cann_Outcome2', 1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Successful')+Cann_Outcome2+prop.getProperty('Cann'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(3)
WebUI.takeScreenshot()














