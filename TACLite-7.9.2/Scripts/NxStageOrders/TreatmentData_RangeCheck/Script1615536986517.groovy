import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.Keys as Keys
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\TreatmentData.properties')

Properties prop1 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\EnvironmentalVariables.properties')

Properties  staticvalues = helperUtilities.getobject('C:\\katalon\\TACLite\\Data Resources\\CS_EnvVariables.properties')

TestObject testObj = new TestObject()
/*
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('BackToCS'), '', 'scrolltoelement')
WebUI.delay(1)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('BackToCS'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
*/
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Treatment_Data'),'','click')
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_Treatment'), '', 'switchframe')


WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Add_Manual'),'','click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Time'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Hemaclip'),'','click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Effluent'),staticvalues.getProperty("EffluentIncorrectRange"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FF'),staticvalues.getProperty("FFCorrectRange"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Dialysate'),staticvalues.getProperty("DialysateVolCorrectRange"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('UF'),staticvalues.getProperty("UF"),'input')
WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'),'','click')
WebUI.delay(2)

//Validate the error message for Effluent Pressure range
def eff=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('EFMessageText'))

if (WebUI.waitForElementPresent(eff, 3, FailureHandling.OPTIONAL)){
	println("Valid error pop up for Effluent Pressure range")
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Ok_button'),'','click')
	WebUI.delay(2)
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Effluent'),staticvalues.getProperty("EffluentCorrectRange"),'input')
	
	
}else{
	WebUI.setAlertText("Error message for Effluent pressure is not coming up if value entered is below are above the especified range.")
}

//Validate the error message for Dialysate Volume Remaining range

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Dialysate'),staticvalues.getProperty("DialysateVolIncorrectRange"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'),'','click')

def dialysate=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('DialysateMessageText'))

if (WebUI.waitForElementPresent(dialysate, 3, FailureHandling.OPTIONAL)){
	println("Valid error pop up for Dialysate Volume Remaining range")
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Ok_button'),'','click')
	WebUI.delay(2)
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Dialysate'),staticvalues.getProperty("DialysateVolCorrectRange"),'input')
	
}else{
	WebUI.setAlertText("Error message for Dialysate Volume Remaining is not coming up if value entered is below are above the especified range.")
}


//Validate the error message for FF% range

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FF'),staticvalues.getProperty("FFIncorrectRange"),'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'),'','click')

def ff=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('FFMessageText'))

if (WebUI.waitForElementPresent(ff, 3, FailureHandling.OPTIONAL)){
	println("Valid error pop up for FF% range")
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Ok_button'),'','click')
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FF'),staticvalues.getProperty("FFCorrectRange"),'input')
	WebUI.delay(2)
}else{
	WebUI.setAlertText("Error message for FF% Range is not coming up if value entered is below are above the especified range.")
}


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'),'','click')
WebUI.delay(2)
CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty("Username"),prop1.getProperty("Password"))

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Continue_button'), '', 'click')

WebUI.takeScreenshot()




