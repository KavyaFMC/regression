import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

 


List<Map<String,String>> data =  [
    
    ["id":"machinenum"],
    ["id":"station"],
    ["id":"dialyzer"],
    ["id":"cartridgeLotNum"],
    ["id":"machine"],
    ["id":"therapyFluid"],
    ["id":"bfr2"],
    ["id":"expressBag"],
    ["id":"expressBagLotNumber"],
    ["id":"pureFlow"],
    ["id":"pfslSerialNumber"],
    ["id":"pakLotNumber"],
    ["id":"sakLotNumber"],
    ["id":"alarmTestPassed"],
    ["id":"newBatchClppm"],
    ["id":"notNewBatch"],
    ["id":"signature"]
    
    
]
List<WebElement> webElementsInPage =
    CustomKeywords.'reusableComponents.VerifyElements.getWebElementsAsList'('//div[@id="editableContainer"]//div//div')
WebUI.comment("webElementsInPage.size()=${webElementsInPage.size()-1}")
for (int i = 0; i < data.size(); i++) {
    data[i].found = 'no'
    for (WebElement el: webElementsInPage) {
        WebElement node = el.findElement(By.xpath('.'))  
        String j = node.getAttribute("id").trim()
        
        //String t = node.getText().trim()
        if (j == data[i].id) {
            data[i].found = 'yes'
        }
    }
}
for (Map m : data) {
    WebUI.comment("${m.id} is displayed:${m.found}")
}

 


List<Map<String,String>> data1 =  [

    ["id":"submitButton"]
     
]
List<WebElement> webElementsInPage1 =
    CustomKeywords.'reusableComponents.VerifyElements.getWebElementsAsList'('//div[@id="editableContainer"]//div//button')
WebUI.comment("webElementsInPage1.size()=${webElementsInPage1.size()}")
for (int i = 0; i < data1.size(); i++) {
    data1[i].found = 'no'
    for (WebElement el: webElementsInPage1) {
        WebElement node = el.findElement(By.xpath('.'))
        String j = node.getAttribute("id").trim()
        
        //String t = node.getText().trim()
        if (j == data1[i].id) {
            data1[i].found = 'yes'
        }
    }
}
for (Map m : data1) {
    WebUI.comment("${m.id} is displayed:${m.found}")
}

 

////div[@id='editableContainer']//div//div[contains(.,'Cycler Serial #:')]