import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor

WebUI.comment('Load the required property files')
Properties propNx = helperUtilities.getNxProp1('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_HDOrders.properties')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx.getProperty('HDOrders_tab'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx.getProperty('HDOrders_frame'), '', 'switchframe')


WebUI.delay(3)
WebDriver driver = DriverFactory.getWebDriver()

WebUI.comment("Verifying elements present on HD Orders screen")

String[] elem = new String[10];
elem[4] = "BFR:";
elem[5] = "Dialyzer:"
elem[6] = "Therapy Fluid (dialysate):"
elem[7] = "Volume per Tx (L):"
elem[8] = "FF %:"
elem[9] = "Special Attention"

for(i=4;i<=9;i++) {
HDOrder_elementcheck=driver.findElement(By.xpath("//*[@id='ext-gen6']/div["+i+"]/p/b"))
HDOrder_elem=driver.findElement(By.xpath("//*[@id='ext-gen6']/div["+i+"]/p/b")).getAttribute("textContent")
HDOrder_elem=HDOrder_elem.trim()
//highlight on the UI screen
JavascriptExecutor js = (JavascriptExecutor)driver;
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",HDOrder_elementcheck)

println(""+HDOrder_elementcheck+" is present on the HD Orders screen")

if (WebUI.verifyEqual(elem[i], HDOrder_elem, FailureHandling.OPTIONAL)) {

	println(""+HDOrder_elem+" is present on the HD Orders screen")
}
else{
	println(""+HDOrder_elem+" is not present on the HD Orders screen")
}


}

WebUI.comment("Verifying element names present on HD Orders screen")

String[] element = new String[3];
element[1] = "Allergies";
element[2] = "Patient Management";


for(i=1;i<=2;i++) {
	
HDOrder_elementscheck=driver.findElement(By.xpath("//*[@id='ext-gen6']/div[11]/table/tbody/tr[1]/td["+i+"]/b"))
HDOrder_element=driver.findElement(By.xpath("//*[@id='ext-gen6']/div[11]/table/tbody/tr[1]/td["+i+"]/b")).getAttribute("textContent")
HDOrder_element=HDOrder_element.trim()
//highlight on the UI screen
JavascriptExecutor js = (JavascriptExecutor)driver;
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",HDOrder_elementscheck)

if (WebUI.verifyEqual(element[i], HDOrder_element, FailureHandling.OPTIONAL)) {

	println(""+HDOrder_element+" is present on the HD Orders screen")
}
else{
	println(""+HDOrder_element+" is not present on the HD Orders screen")
}

}
WebUI.takeScreenshot()

