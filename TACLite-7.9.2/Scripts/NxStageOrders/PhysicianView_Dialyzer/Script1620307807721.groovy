import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor

WebUI.comment('Load the required property files')
Properties propNx = helperUtilities.getNxProp1('C://katalon//TACLite//Data Resources//NxStageOrders//NxStage_PhysicianView.properties')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx.getProperty('PhysicianView'), '', 'click')

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(propNx.getProperty('Frame_PhyView'), '', 'switchframe')

WebDriver driver = DriverFactory.getWebDriver()
//Headers verification
WebUI.comment("Verifying Headers present on Physician View screen")

String[] elem = new String[4];
elem[2] = "Current Treatment Actions";
elem[3] = "Access"

for(i=2;i<=3;i++) {
	WebUI.delay(2)
HeadersCheck=driver.findElement(By.xpath("//*[@id='physicianViewForm']/div["+i+"]/p/b"))
Headers_elem=driver.findElement(By.xpath("//*[@id='physicianViewForm']/div["+i+"]/p/b")).getAttribute("textContent")
println(Headers_elem)

Headers_elem=Headers_elem.trim()
//highlight on the UI screen
JavascriptExecutor js = (JavascriptExecutor)driver;
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",HeadersCheck)

println(""+HeadersCheck+" is present on the Physician View screen")

if (WebUI.verifyEqual(elem[i], Headers_elem, FailureHandling.OPTIONAL)) {

	println(""+Headers_elem+" is present on the Physician View screen")
}
else{
	println(""+Headers_elem+" is not present on the Physician View screen")
}


}
WebUI.takeScreenshot()
//End headers verification

//HD Prescription verification
WebUI.comment("Verifying element names present on Physician View screen-HD Prescription")

HD_Pres="HD Prescription"
HD_Prescheck=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr[1]/td"))
HD_Pres_element=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr[1]/td")).getAttribute("textContent")
HD_Pres_element=HD_Pres_element.trim()

String[] HDPres_Text = HD_Pres_element.split(" ");
String HDPres_Text1 = HDPres_Text[0]; 
String HDPres_Text2 = HDPres_Text[1];
println(HDPres_Text1)
println(HDPres_Text2)

HD_Pres_screentext=HDPres_Text1+" "+HDPres_Text2
println(HD_Pres_screentext)
//highlight on the UI screen
JavascriptExecutor js1 = (JavascriptExecutor)driver;
js1.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",HD_Prescheck)
 
if (WebUI.verifyEqual(HD_Pres, HD_Pres_screentext, FailureHandling.OPTIONAL)) {
println(""+HD_Pres_screentext+" is present on the HD Orders screen")
}
 
else{
println(""+HD_Pres_screentext+" is not present on the HD Orders screen")
}

//End HD Prescription verification

// Dialyzer verification
WebUI.comment("Verifying element names present on Physician View screen-Dialyzer,Volume,Treatment status")

String[] elem1 = new String[5];
elem1[2] = "Dialyzer"
elem1[3] = "Volume per Tx (L)"
elem1[4] = "Treatment Status"

for(i=2;i<=4;i++) {
	WebUI.delay(2)
	
DialyzerCheck=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr["+i+"]/td[1]"))
DialyzerCheck_elem=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr["+i+"]/td[1]")).getAttribute("textContent")
DialyzerCheck_elem=DialyzerCheck_elem.trim()

String[] Dialyzer_Text = DialyzerCheck_elem.split(":");
String Dialyzer_Text1 = Dialyzer_Text[0].trim()


//highlight on the UI screen
JavascriptExecutor js = (JavascriptExecutor)driver;
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",DialyzerCheck)


if (WebUI.verifyEqual(elem1[i], Dialyzer_Text1, FailureHandling.OPTIONAL)) {

	println(""+Dialyzer_Text1+" is present on the Physician View screen")
}
else{
	println(""+Dialyzer_Text1+" is not present on the Physician View screen")
}


}


//Therapy fluid,FF%,Hours on verification 
WebUI.comment("Verifying element names present on Physician View screen-Therapy fluid,FF%,Hours on")

String[] elem2 = new String[5];
elem2[2] = "Therapy Fluid (dialysate)"
elem2[3] = "FF%"
elem2[4] = "Hours On"

for(i=2;i<=4;i++) {
	WebUI.delay(2)
	
TherapyFluid=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr["+i+"]/td[2]"))
TherapyFluid_elem=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr["+i+"]/td[2]")).getAttribute("textContent")
TherapyFluid_elem=TherapyFluid_elem.trim()

String[] TherapyFluid_Text = TherapyFluid_elem.split(":");
String TherapyFluid_Text1 = TherapyFluid_Text[0].trim()


//highlight on the UI screen
JavascriptExecutor js = (JavascriptExecutor)driver;
js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",TherapyFluid)


if (WebUI.verifyEqual(elem2[i], TherapyFluid_Text1, FailureHandling.OPTIONAL)) {

	println(""+TherapyFluid_Text1+" is present on the Physician View screen")
}
else{
	println(""+TherapyFluid_Text1+" is not present on the Physician View screen")
}


}

//End Therapy fluid,FF%,Hours on verification

//spKtV,Ordered BFR,wkstdKtV verification

WebUI.comment("Verifying element names present on Physician View screen-spKtV,Ordered BFR,wkstdKtV")
//spKtV
spKtV = "spKt/V Daugirdas (HHD and NxStage)"
spKtVcheck=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr[3]/td[3]"))
spKtVcheck_element=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr[3]/td[3]")).getAttribute("textContent")
spKtVcheck_element=spKtVcheck_element.trim()

String[] spKtV_Text = spKtVcheck_element.split(":");
String spKtV_Text1 = spKtV_Text[0];

//highlight on the UI screen
JavascriptExecutor js3 = (JavascriptExecutor)driver;
js3.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",spKtVcheck)


//OrderedBFR
OrderedBFR = "Ordered BFR"
OrderedBFRcheck=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr[2]/td[3]"))
OrderedBFRcheck_element=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr[2]/td[3]")).getAttribute("textContent")
OrderedBFRcheck_element=OrderedBFRcheck_element.trim()

String[] OrderedBFR_Text = OrderedBFRcheck_element.split(":");
String OrderedBFR_Text1 = OrderedBFR_Text[0];

//highlight on the UI screen
JavascriptExecutor js4 = (JavascriptExecutor)driver;
js4.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",OrderedBFRcheck)


//wkstdKtV
wkstdKtV = "wkstdKt/V"
wkstdKtVcheck=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr[3]/td[4]"))
wkstdKtVcheck_element=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[1]/table/tbody/tr[1]/td/table/tbody/tr[3]/td[4]")).getAttribute("textContent")
wkstdKtVcheck_element=wkstdKtVcheck_element.trim()

String[] wkstdKtV_Text = wkstdKtVcheck_element.split(":");
String wkstdKtV_Text1 = wkstdKtV_Text[0];

//highlight on the UI screen
JavascriptExecutor js5 = (JavascriptExecutor)driver;
js5.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",wkstdKtVcheck)


if (WebUI.verifyEqual(spKtV, spKtV_Text1, FailureHandling.OPTIONAL)) {
println(""+spKtV_Text1+" is present on the Physician View screen")
}
 
else{
println(""+spKtV_Text1+" is not present on the Physician View screen")
}

if (WebUI.verifyEqual(OrderedBFR, OrderedBFR_Text1, FailureHandling.OPTIONAL)) {
println(""+OrderedBFR_Text1+" is present on the Physician View screen")
}
 
else{
println(""+OrderedBFR_Text1+" is not present on the Physician View screen")
}

if (WebUI.verifyEqual(wkstdKtV, wkstdKtV_Text1, FailureHandling.OPTIONAL)) {
	println(""+wkstdKtV_Text1+" is present on the Physician View screen")
}
 
else{
println(""+wkstdKtV_Text1+" is not present on the Physician View screen")
}
WebUI.takeScreenshot()
//End spKtV,Ordered BFR,wkstdKtV verification



//Pre treatment vitals verification

WebUI.delay(3)
WebUI.comment("Verifying element names present on Physician View screen-Pre treatment vitals")

Pre_Tx="Pre Treatment Vitals"
Pre_Txcheck=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[2]/table/tbody/tr[1]/td"))
Pre_Tx_element=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[2]/table/tbody/tr[1]/td")).getAttribute("textContent")
Pre_Tx_element=Pre_Tx_element.trim()

//highlight on the UI screen
JavascriptExecutor js6 = (JavascriptExecutor)driver;
js6.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Pre_Txcheck)
 
if (WebUI.verifyEqual(Pre_Tx, Pre_Tx_element, FailureHandling.OPTIONAL)) {
println(""+Pre_Tx_element+" is present on the HD Orders screen")
}
 
else{
println(""+Pre_Tx_element+" is not present on the HD Orders screen")
}

//End Pre tx vitals verification

//HD tx details,Administered Medications verification

WebUI.delay(3)

WebUI.comment("Verifying element names present on Physician View screen-HD tx details,Administered Medications")

String[] elem4 = new String[3];
elem4[1] = "HD Treatment Details";
elem4[2] = "Administered Medications"

for(i=1;i<=2;i++) {
	WebUI.delay(2)
HDMed_check=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[3]/table/tbody/tr[1]/td["+i+"]/table/tbody/tr[1]/td"))
HDMed_elem=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[3]/table/tbody/tr[1]/td["+i+"]/table/tbody/tr[1]/td")).getAttribute("textContent")
println(HDMed_elem)

HDMed_elem=HDMed_elem.trim()
//highlight on the UI screen
JavascriptExecutor js7 = (JavascriptExecutor)driver;
js7.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",HDMed_check)


if (WebUI.verifyEqual(elem4[i], HDMed_elem, FailureHandling.OPTIONAL)) {

	println(""+HDMed_elem+" is present on the Physician View screen")
}
else{
	println(""+HDMed_elem+" is not present on the Physician View screen")
}


}

//End HD tx details,Administered Medications verification


//Post treatment vitals verification

WebUI.delay(3)

WebUI.comment("Verifying element names present on Physician View screen-Post treatment vitals")

Post_Tx="Post Treatment Vitals"
Post_Txcheck=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[4]/table/tbody/tr[1]/td"))
Post_Tx_element=driver.findElement(By.xpath("//*[@class='fmcScrollContent']/div/div[4]/table/tbody/tr[1]/td")).getAttribute("textContent")
Post_Tx_element=Post_Tx_element.trim()

//highlight on the UI screen
JavascriptExecutor js8 = (JavascriptExecutor)driver;
js8.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;')",Post_Txcheck)
 
if (WebUI.verifyEqual(Post_Tx, Post_Tx_element, FailureHandling.OPTIONAL)) {
println(""+Post_Tx_element+" is present on the HD Orders screen")
}
 
else{
println(""+Post_Tx_element+" is not present on the HD Orders screen")
}

WebUI.takeScreenshot()
//End Post treatment vitals verification








