import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import java.time.LocalDateTime as LocalDateTime
import java.time.format.DateTimeFormatter as DateTimeFormatter
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import reusablekeyword.Reusable as Reusable
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\PreDialysis.properties')

Properties prop1 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\EnvironmentalVariables.properties')

Properties staticvalues = helperUtilities.getobject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\CS_EnvVariables.properties')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreDialysis'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Update_button1'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

WebUI.callTestCase(findTestCase('CommonFunctions/writeValues'), [:], FailureHandling.STOP_ON_FAILURE)

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet("C:\\katalon\\TACLite\\InputData\\ReadWrite.xlsx", "Data", true)

def PreWeight = excelData.getValue('PreWeight', 1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreWeight'), ''+PreWeight, 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Resp'), staticvalues.getProperty('RespNo'), 'input')

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('O2'), staticvalues.getProperty('02No'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SitBP'), staticvalues.getProperty('SitBPNo'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SitDiastolic'), staticvalues.getProperty('SitDiastolicNo'), 
    'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Temp'), staticvalues.getProperty('Tempvalue'), 
    'input')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Confirmation1'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Confirmation2'), '', 'click')

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('StandingBP'), staticvalues.getProperty('StandingBPNo'), 
    'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('StandingDiastolic'), staticvalues.getProperty('StandingDiastolicNo'), 
    'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pulse'), staticvalues.getProperty('PulseNo'), 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('IR'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TargetWeight'), staticvalues.getProperty('TargetWeight'), 
    'input')



WebUI.callTestCase(findTestCase('CommonFunctions/writeTimeValues'), [:], FailureHandling.STOP_ON_FAILURE)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TimeObserved'), '', 'click')
Object excelData1 = ExcelFactory.getExcelDataWithDefaultSheet("C:\\katalon\\TACLite\\InputData\\ReadWrite.xlsx", "Data1", true)

def Time = excelData1.getValue('Time', 1)

println(Time)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TimeObserved'), '' + Time, 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty('Username'), prop1.getProperty('Password'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Continue'), '', 'click')

WebUI.delay(2)



//----------------------------------Update button 2-----------------------------------------------
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Update_button2'), '', 'click')
WebUI.delay(5)
CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

WebUI.delay(3)
WebUI.comment('Select No unusual findings')  
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Findings'), '', 'click')

//WebUI.comment('Added for FMS requirements-Shortness of breath checkbox selection')

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Shortness'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TimeObserved2'), '' + Time, 'input')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty('Username'), prop1.getProperty('Password'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Continue'), '', 'click')

WebUI.delay(2)

WebUI.takeScreenshot()


