import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.testdata.reader.ExcelFactory
import java.io.IOException
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities

import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.testobject.ConditionType

TestObject testObj = new TestObject()

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\Cannulation.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CannDoc'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('CannDoc'), '', 'click')
WebUI.delay(3)
WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Select1'), '', 'click')
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_NoFindings', true)
def CannCleaned1=excelData.getValue('CannCleaned1', 3)
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_AccessCleaned')+CannCleaned1+prop.getProperty('Cann'), '', 'click')

WebUI.comment('Check if buttonhole checkbox is present')

def buttonhole=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Buttonhole'))

if (WebUI.waitForElementPresent(buttonhole, 3, FailureHandling.OPTIONAL)){
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Buttonhole'), '', 'click')
}
else{
	WebUI.comment('Buttonhole checkbox is not present')
}


def selfcann=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('SelfCannulation'))

if (WebUI.waitForElementPresent(selfcann, 3, FailureHandling.OPTIONAL)){
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('SelfCannulation'), '', 'click')
}
else{
	WebUI.comment("This is error-Self Cannulation should be available for AVF/AVG access")
	WebUI.closeBrowser()
}

def ultrasound=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('UltraSound'))

if (WebUI.waitForElementPresent(ultrasound, 3, FailureHandling.OPTIONAL)){
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('UltraSound'), '', 'click')
}
else{
	WebUI.comment("This is error-UltraSound should be available for AVF/AVG access")
	WebUI.closeBrowser()
}


def ArterialAttempts=excelData.getValue('ArterialAttempts', 3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialAttempts1')+ArterialAttempts+prop.getProperty('Cann'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FinalAttempt1'), '', 'click')

WebUI.delay(5)
def ArterialNeedleSize=excelData.getValue('ArterialNeedleSize', 3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialNeedleSize')+ArterialNeedleSize+prop.getProperty('Cann'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('ArterialNeedleSize')+ArterialNeedleSize+prop.getProperty('Cann'), '', 'click')
def Cann_Outcome1=excelData.getValue('Cann_Outcome1', 3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Cann_Successful')+Cann_Outcome1+prop.getProperty('Cann'), '', 'click')

WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(3)




















