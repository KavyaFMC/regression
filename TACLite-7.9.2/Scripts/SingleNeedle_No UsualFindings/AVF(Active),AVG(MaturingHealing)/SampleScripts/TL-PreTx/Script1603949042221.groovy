import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import java.io.IOException
import com.kms.katalon.core.testdata.reader.ExcelFactory
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import com.kms.katalon.core.testobject.ConditionType as ConditionType
Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//ChairSide_Properties//EnvironmentalVariables.properties')


TestObject testObj=new TestObject()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'scrolltoelement')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'click')
WebUI.delay(3)

def post = testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Closebtn'))
WebUI.comment("Check for appointment reminders and click on close button")

if (WebUI.waitForElementPresent(post, 3, FailureHandling.OPTIONAL)) {
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Closebtn'), '', 'click')
} else {
	println('Proceed to next-do nothing')
}

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select1'), '', 'click')

WebUI.comment("Check if post-surgical assessment is present")

WebUI.callTestCase(findTestCase('CommonFunctions/PostSurgical'), [:], FailureHandling.OPTIONAL)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_NoFindings', true)

def PreTx_Look1=excelData.getValue('PreTx_Look1', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look1+prop.getProperty('Pre'), '', 'click')

def PreTx_Listen1=excelData.getValue('PreTx_Listen1', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen1+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)
def PreTx_Feel1=excelData.getValue('PreTx_Feel1', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel1+prop.getProperty('Pre'), '', 'click')

def PreTx_Other1=excelData.getValue('PreTx_Other1', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other1+prop.getProperty('Pre'), '', 'click')

WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select2'), '', 'click')

WebUI.comment("Check if post-surgical assessment is present")

WebUI.callTestCase(findTestCase('CommonFunctions/PostSurgical'), [:], FailureHandling.OPTIONAL)

def PreTx_Look2=excelData.getValue('PreTx_Look2', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Look1')+PreTx_Look2+prop.getProperty('Pre'), '', 'click')

def PreTx_Listen2=excelData.getValue('PreTx_Listen2', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Listen1')+PreTx_Listen2+prop.getProperty('Pre'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('PreOther'), '', 'scrolltoelement')

WebUI.delay(3)
def PreTx_Feel2=excelData.getValue('PreTx_Feel2', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Feel1')+PreTx_Feel2+prop.getProperty('Pre'), '', 'click')
def PreTx_Other2=excelData.getValue('PreTx_Other2', 4)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other2+prop.getProperty('Pre'), '', 'click')
WebUI.takeScreenshot()
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(2)






