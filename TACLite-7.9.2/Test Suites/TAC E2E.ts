<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TAC E2E</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>afd00c3e-f8f5-40fb-aa26-715b56aeb66d</testSuiteGuid>
   <testCaseLink>
      <guid>31ce3073-25f4-4cfc-b7bf-2352184b24c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/Final AVF(Active),AVF(TemporarilyUnUsable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7997d15e-125b-4673-a1ec-ad7301ebe7e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b4934f2-ac1b-4459-833d-fbaedf830602</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49b21049-405a-4416-93bc-fcd85892e882</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_No UsualFindings/CVC(Active),CVC(WaitingForRemoval)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>816c1bb3-8c11-43a4-b0d4-43aca76263bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVF(Active),AVF(Temporarily Unusable)/Final AVF(Active),AVF(Temporarily Unusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8145753b-257f-4e7b-b063-56a0ce5ca917</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>505fc29e-41f8-46e2-a5e6-d8035f7ffd9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8169bd11-5d7b-49ef-a0bb-1453222745d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BothNeedle_WithFindings/CVC(Active),CVC(WaitingForRemoval)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c13d4796-3358-4ad4-be31-d0b10326617c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/Final-AVF(Active),AVF(TemporaryUnusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f5eec55-c1de-40df-a962-bf883a0696be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a780873-595a-45ba-8898-b6f3e18278e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f26ec0bd-a672-4009-8493-c9c15b0a62ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/CVC(Active),CVC(WaitingForRemoval)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7639fc5-0bb0-4c03-aadc-a57f57c47179</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVF(Temporarily Unusable)/Final AVF(Active),AVF(Temporarily Unusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fad9730a-6aab-4f87-9647-3294f1099dbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95f9dc0a-1375-4b69-b448-f08d2fa9211e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),CVC(WaitingForRemoval)/Final AVF(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1333ab50-989f-45a1-addf-9c13686b70a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/CVC(Active),CVC(Waiting for Removal)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
