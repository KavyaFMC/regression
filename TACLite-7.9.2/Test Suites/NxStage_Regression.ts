<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>NxStage_Regression</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b53c03ad-60a0-49bf-8ad8-4a680479fbef</testSuiteGuid>
   <testCaseLink>
      <guid>280b9487-2e98-4260-a0cb-347c09b152d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/NxStageOrders/NxStage_RegressionSuite/CVC(Active)CVC(WaitingForRemoval)-SingleNoFindings/FinalCVC(Active)CVC(WaitingForRemoval)-SingleNoFindings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e575ce38-8ad1-49cf-92f7-42438c3b6b9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/NxStageOrders/NxStage_RegressionSuite/AVG(Active),AVF(TempUnusable)-SingleWithFindings/Final AVF(Active),AVF(Temporarily Unusable)-SingleWithFindings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce9cf0c3-2cc0-4806-a389-40899f2129f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/NxStageOrders/NxStage_RegressionSuite/AVG(Active)AVF(MattHealing)-BothWithFindings/FinalAVG(Active)AVF(MattHealing)-BothWithFindings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3162db50-ea80-4770-bd3c-09f6b018cff4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/NxStageOrders/NxStage_RegressionSuite/AVG(Active)CVC(WaitingForRemoval)-BothNoFindings/FinalAVG(Active)CVC(WaitingForRemoval)-BothNoFindings</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
