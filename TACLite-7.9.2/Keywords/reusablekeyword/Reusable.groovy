package reusablekeyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.testobject.ConditionType
import utilityKeywords.helperUtilities as helperUtilities
import internal.GlobalVariable
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.YearMonth
import java.time.format.DateTimeFormatter



public class Reusable {
	Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\PatientSelection.properties')
	Properties prop2= helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\AddAccess.properties')
	Properties prop3 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\EnvironmentalVariables.properties')
	Properties prop1= helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\MachineSetup.properties')
	Properties prop4 = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')


	@Keyword
	public void datepicker(String xpath){
		YearMonth thisMonth = YearMonth.now()
		YearMonth lastMonth = thisMonth.minusMonths(1)
		//println lastMonth

		DateTimeFormatter abcx=DateTimeFormatter.ofPattern("MM")
		def finalmonth=lastMonth.format(abcx)
		println finalmonth

		DateFormat dateformat1=new SimpleDateFormat("dd")
		Date date= new Date()
		String todaydate=dateformat1.format(date)
		println todaydate


		DateFormat dateformat2=new SimpleDateFormat("yyyy")
		Date year= new Date()
		String Currentyear=dateformat2.format(year)
		println Currentyear

		println finalmonth+'/'+todaydate+'/'+Currentyear

		def DatePicker=finalmonth+'/'+todaydate+'/'+Currentyear
		println DatePicker

		TestObject testObj=new TestObject()
		testObj.addProperty("xpath", ConditionType.EQUALS, (xpath))

		WebUI.setText(testObj,DatePicker)

	}

	@Keyword
	public void TACLogin(){

		TestObject testObj=new TestObject()
		testObj.addProperty("xpath", ConditionType.EQUALS, prop.getProperty('Username'))
		WebUI.setText(testObj,prop3.getProperty("Username"));
		testObj.addProperty("xpath", ConditionType.EQUALS, prop.getProperty('Password'))
		WebUI.setText(testObj,prop3.getProperty("Password"));
		testObj.addProperty("xpath", ConditionType.EQUALS, prop.getProperty('Submit'))
		WebUI.click(testObj);
	}
	@Keyword
	public void CS(String Username,String Password){
		TestObject testObj=new TestObject()
		testObj.addProperty("xpath", ConditionType.EQUALS, prop1.getProperty('Performed_User'))
		WebUI.setText(testObj,Username);

		testObj.addProperty("xpath", ConditionType.EQUALS, prop1.getProperty('Performed_Sign'))
		WebUI.setText(testObj,Password);
	}

	@Keyword
	public void TACSignature(){
		TestObject testObj=new TestObject()
		testObj.addProperty("xpath", ConditionType.EQUALS,prop2.getProperty('Signature_TAC') )
		WebUI.scrollToElement(testObj,10 )
		testObj.addProperty("xpath", ConditionType.EQUALS,prop2.getProperty('Signature_TAC'))
		WebUI.click(testObj);
		WebUI.delay(3)
		testObj.addProperty("xpath", ConditionType.EQUALS, prop2.getProperty('TAC_SignUser'))
		WebUI.setText(testObj,prop3.getProperty("User"));
		testObj.addProperty("xpath", ConditionType.EQUALS, prop2.getProperty('TAC_SignPassword'))
		WebUI.setText(testObj,prop3.getProperty("Password"));
		testObj.addProperty("xpath", ConditionType.EQUALS, prop2.getProperty('ContinueButton') )
		WebUI.click(testObj);
	}
	@Keyword
	public void STARTTX(){
		TestObject testObj=new TestObject()
		testObj.addProperty("xpath", ConditionType.EQUALS,prop4.getProperty('BackToCS'))
		WebUI.scrollToElement(testObj,10 )
		WebUI.delay(1)
		testObj.addProperty("xpath", ConditionType.EQUALS,prop4.getProperty('BackToCS'))
		WebUI.click(testObj);
		WebUI.delay(2)
		WebUI.switchToDefaultContent()
		testObj.addProperty("xpath", ConditionType.EQUALS,prop4.getProperty('StartTx'))
		WebUI.scrollToElement(testObj,10 )
		WebUI.delay(2)
		testObj.addProperty("xpath", ConditionType.EQUALS,prop4.getProperty('StartTx'))
		WebUI.click(testObj);
		WebUI.delay(3)
		testObj.addProperty("xpath", ConditionType.EQUALS, prop1.getProperty('Performed_User'))
		WebUI.setText(testObj,prop3.getProperty("Username"));
		testObj.addProperty("xpath", ConditionType.EQUALS, prop1.getProperty('Performed_Sign'))
		WebUI.setText(testObj,prop3.getProperty("Password"));
	}
	@Keyword
	public void TxStart(){
		TestObject testObj=new TestObject()

		testObj.addProperty("xpath", ConditionType.EQUALS, prop1.getProperty('Performed_User'))
		WebUI.setText(testObj,prop3.getProperty("Username"));
		testObj.addProperty("xpath", ConditionType.EQUALS, prop1.getProperty('Performed_Sign'))
		WebUI.setText(testObj,prop3.getProperty("Password"));
	}
}
