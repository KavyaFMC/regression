<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Single needle - With Findings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b5c6ee57-2e96-457d-9379-8b76283f82d0</testSuiteGuid>
   <testCaseLink>
      <guid>f40c7b12-07ed-4e58-a227-c351471a312e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVF(Temporarily Unusable)/Final AVF(Active),AVF(Temporarily Unusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>800696ee-df8e-4b8f-91c6-8c493fbe028b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc9ca569-a6fc-4315-b887-13afd57f8936</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/AVF(Active),CVC(WaitingForRemoval)/Final AVF(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54f5f4ff-93d9-4eee-b732-0f118cf78872</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleNeedle_WithFindings/CVC(Active),CVC(Waiting for Removal)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
