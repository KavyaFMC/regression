<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Single needle-No UsualFindings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>76685416-d82e-48f8-b222-dc2d92d4ad34</testSuiteGuid>
   <testCaseLink>
      <guid>d85e957a-0c0c-41a8-96fe-df167976c777</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/AVF(Active),AVF(TemporarilyUnUsable)/Final-AVF(Active),AVF(TemporaryUnusable)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af735b82-c85d-4c4d-ba80-5ba8f3108da7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/AVF(Active),AVG(MaturingHealing)/Final AVF(Active),AVG(MaturingHealing)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6045ed03-3c44-4d7b-9ffc-f4cc40ec6559</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/Final AVG(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65fe3ea1-0c7b-4b0a-84c3-43b0ea8510f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleNeedle_No UsualFindings/CVC(Active),CVC(WaitingForRemoval)/Final CVC(Active),CVC(WaitingForRemoval)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
