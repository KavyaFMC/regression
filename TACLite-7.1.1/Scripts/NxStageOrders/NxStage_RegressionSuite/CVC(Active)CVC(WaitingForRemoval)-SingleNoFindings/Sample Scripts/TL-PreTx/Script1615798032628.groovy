import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues

import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import java.io.IOException as IOException
import java.time.LocalDateTime as LocalDateTime
import java.time.format.DateTimeFormatter as DateTimeFormatter

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\PreTreatment.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'scrolltoelement')

WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Tx'), '', 'click')

WebUI.delay(3)
WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select1'), '', 'click')

WebUI.delay(3)
Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_NoFindings', true)
def PreTx_Other1=excelData.getValue('PreTx_Other1', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other1+prop.getProperty('Pre'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Select2'), '', 'click')

def PreTx_Other2=excelData.getValue('PreTx_Other1', 2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Pre_Other1')+PreTx_Other2+prop.getProperty('Pre'), '', 'click')
CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(2)
WebUI.takeScreenshot()



