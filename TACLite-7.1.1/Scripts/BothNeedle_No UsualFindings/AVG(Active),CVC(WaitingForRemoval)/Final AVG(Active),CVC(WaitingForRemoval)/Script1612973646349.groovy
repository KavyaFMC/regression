import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('CommonFunctions/clearValues'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/SampleScript/CS_PatientSelection'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/ChairSideScripts/CS_MachineSetup'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/ChairSideScripts/CS_PreDialysis'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/SampleScript/TL-AddAccess'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/SampleScript/TL-PreTx'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/SampleScript/CS_StartTx'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/SampleScript/TL-CannDoc'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/SampleScript/TL_AFT'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/SampleScript/TL-DuringPost'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('BothNeedle_No UsualFindings/AVG(Active),CVC(WaitingForRemoval)/SampleScript/CS_PostDialysis'), 
    [:], FailureHandling.STOP_ON_FAILURE)



