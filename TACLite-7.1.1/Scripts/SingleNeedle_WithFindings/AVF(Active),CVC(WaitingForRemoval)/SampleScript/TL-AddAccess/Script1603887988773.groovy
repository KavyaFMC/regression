import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import utilityKeywords.helperUtilities as helperUtilities

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\AddAccess.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Access_Eval'), '', 'click')
WebUI.delay(2)
WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
WebUI.delay(2)


CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_Findings', true)
def AccessStatus1=excelData.getValue('AccessStatus1', 3)
println(AccessStatus1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Select')+AccessStatus1+prop.getProperty('Selectbutton'), '', 'click')


def LineSource1=excelData.getValue('LineSource1', 3)
println(LineSource1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Arterial')+LineSource1+prop.getProperty('Add'), '', 'click')
	
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('OKbutton'), '', 'click')



WebUI.delay(2)


def AccessStatus2=excelData.getValue('AccessStatus2', 3)
println(AccessStatus2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Select')+AccessStatus2+prop.getProperty('Selectbutton'), '', 'click')


def LineSource2=excelData.getValue('LineSource2', 3)
println(LineSource2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Arterial')+LineSource2+prop.getProperty('Add'), '', 'click')
	
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('OKbutton'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(2)










