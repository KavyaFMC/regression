import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.io.IOException
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testdata.InternalData
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import java.sql.Driver
import java.util.concurrent.TimeUnit

import com.kms.katalon.core.testobject.ConditionType
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import utilityKeywords.helperUtilities as helperUtilities
import java.lang.CharSequence as CharSequence
import com.kms.katalon.core.testobject.ConditionType

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//ChairSide.properties')


CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Interventions'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Interventions'), '', 'click')
println "Started: Clicking on Select Buttons"


int btnNumber=1

while(btnNumber>0){
	
	WebDriver driver = DriverFactory.getWebDriver()
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS)
	driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS)
	//driver.navigate().refresh()
	List<WebElement> listBtn = driver.findElements(By.xpath("//div[@class='cs-table']/div/div/button"))
	/*if(listBtn.size()==0){
		println "Buttons count is: "+listBtn.size()
		println "LoopOut"
		break
	}
	*/
	
	if(listBtn.size()>=btnNumber){
		println "Clicking on Select Button :"+(btnNumber+1)
		listBtn.get(btnNumber-1).click()
		
		Thread.sleep(8000)
		
		
		//select checkboxes and click on "Recannulate"
		List<WebElement>  eleCheckBox = driver.findElements(By.xpath('//*[@id="interventionContainer"]/div/div/div//cs-checkbox-button'))
		println eleCheckBox.size()
		int CheckboxSize=eleCheckBox.size()
		
		for (WebElement x : eleCheckBox) {
			
			println'@'+x.getText()


		if(x.getText().equals('Obtain Culture')){
			x.click()
			break
		}
		//added
		break
		}
		
		
		
		List<WebElement>  eleCheckBox1 = driver.findElements(By.xpath("//div[@id='interventionContainer']/div/div/div[1]//button"))
		 println eleCheckBox1.size()
		
		 for (WebElement y : eleCheckBox) {
			 
			 println'@'+y.getText()
		 if(y.getText().equals('Use Alternative Active Access')){
			 //WebElement eleCheckBox2 = driver.findElement(By.xpath("//div[@id='interventionContainer']/div/div/div[2]//button"))
		//eleCheckBox2.click()
			 break
		 }
		 //added
		 break
		 
		 }
		
		WebElement eleCheckBox3 = driver.findElement(By.xpath("//*[@id='Consult With Nephrologist /NP /PA']"))
		eleCheckBox3.click()
			
			TestObject testObj= new TestObject()
			testObj.addProperty("xpath", ConditionType.EQUALS, "//*[@id='summarySignatureBtn']")
			WebUI.scrollToElement(testObj, 3)
			WebUI.doubleClick(testObj)
			
		//driver.findElement(By.xpath("//*[@id='summarySignatureBtn']")).click()
		Thread.sleep(3000)
		driver.findElement(By.xpath("//*[@id='username']")).sendKeys("Tnurse")
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys("Today123")
		driver.findElement(By.xpath("//*[@id='continueBtn']")).click()
	btnNumber++
}
	else{
		break
	}
	
	
	
	//if (listBtn.size()>btnNumber){
		TestObject testObj2= new TestObject()
		testObj2.addProperty("xpath", ConditionType.EQUALS, '//*[@id="interventionsLanding"]/cs-scrollbar/div/div/button[3]/span')
		WebUI.click(testObj2)
		//WebUI.click(testObj2)
		
	
	
	}
	
WebUI.takeScreenshot()
//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('BackToCS'), '', 'click')








