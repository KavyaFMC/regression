import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import utilityKeywords.helperUtilities as helperUtilities
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.IOException

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\AFT.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')


CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AFT'), '', 'scrolltoelement')
WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AFT'), '', 'click')
WebUI.delay(3)

WebUI.takeScreenshot()
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AFT_Select1'), '', 'click')
WebUI.delay(3)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AFT_OLC'), '', 'click')


Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'BothNeedle_NoFindings', true)
def AFT1=excelData.getValue('AFT1', 4)
println(AFT1)
/*
String[] arr=String.valueOf(AFT1).split("\\.");
int[] intArr=new int[2];
intArr[0]=Integer.parseInt(arr[0]);
intArr[1]=Integer.parseInt(arr[1]);
AFT1=+intArr[0]
*/
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('AFT_Text'), ''+AFT1, 'input')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()
WebUI.delay(2)
WebUI.takeScreenshot()




