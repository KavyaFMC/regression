import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import java.time.LocalDateTime as LocalDateTime
import java.time.format.DateTimeFormatter as DateTimeFormatter
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import reusablekeyword.Reusable as Reusable
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\FluidManagement.properties')

Properties prop1 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\EnvironmentalVariables.properties')

Properties staticvalues = helperUtilities.getobject('C:\\katalon\\TACLite\\Data Resources\\CS_EnvVariables.properties')


/*
CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('StartTx'), '', 'click')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Fluid_frame'), '', 'switchframe')
*/


CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Fluid_footer'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Fluid_button'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
//click on the buttons

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('FM_button'), '', 'click')
WebUI.delay(3)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Symptoms'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Symptoms'), '', 'click')
WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Lungs'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Lungs'), '', 'click')
WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('No_Edema'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('No_Edema'), '', 'click')

TestObject testObj = new TestObject()

def a=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('NotApplicable'))

if (WebUI.waitForElementPresent(a, 3, FailureHandling.OPTIONAL)){
	WebUI.delay(2)
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('NotApplicable'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('NotApplicable'), '', 'click')
}
else{
	println("Do nothing")
}

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'), '', 'click')

def b=testObj.addProperty('xpath', ConditionType.EQUALS, prop1.getProperty('Username'))
if (WebUI.waitForElementPresent(b, 3, FailureHandling.OPTIONAL)){

CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty('Username'), prop1.getProperty('Password'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Continue'), '', 'click')

WebUI.delay(2)
}
//CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

//CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Signature'), '', 'scrolltoelement')
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Sign_FM'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Sign_FM'), '', 'click')
WebUI.delay(2)
CustomKeywords.'reusablekeyword.Reusable.CS'(prop1.getProperty('Username'), prop1.getProperty('Password'))

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Continue'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

def c=testObj.addProperty('xpath', ConditionType.EQUALS, prop.getProperty('Ok_button'))
if (WebUI.waitForElementPresent(c, 3, FailureHandling.OPTIONAL)){

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Ok_button'), '', 'click')

}

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('StartTx'), '', 'scrolltoelement')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('StartTx'), '', 'click')





