import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.IOException

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import utilityKeywords.helperUtilities as helperUtilities
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import excelHandle.excelValues as excelValues
import excelHandle.excelGetValues as excelGetValues
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testdata.InternalData
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import org.openqa.selenium.By as By

import com.kms.katalon.core.testdata.reader.ExcelFactory

Properties prop = helperUtilities.getTheObject('C://katalon//TACLite//Data Resources//TacLite_Properties//AddAccess.properties')

Properties prop1 = helperUtilities.getTheObj('C://katalon//TACLite//Data Resources//EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Access_Eval'), '', 'click')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'('','','defaultcontent')
WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

WebUI.takeScreenshot()

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Katalon.Testdata.xlsx', 'SingleNeedle_NoFindings', true)
def AccessStatus1=excelData.getValue('AccessStatus1', 1)
println(AccessStatus1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Select')+AccessStatus1+prop.getProperty('Selectbutton'), '', 'click')


def LineSource1=excelData.getValue('LineSource1', 1)
println(LineSource1)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Arterial')+LineSource1+prop.getProperty('Add'), '', 'click')
	
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('OKbutton'), '', 'click')

//CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(2)


def AccessStatus2=excelData.getValue('AccessStatus2', 1)
println(AccessStatus2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Select')+AccessStatus2+prop.getProperty('Selectbutton'), '', 'click')


def LineSource2=excelData.getValue('LineSource2', 1)
println(LineSource2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Arterial')+LineSource2+prop.getProperty('Add'), '', 'click')
	
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('OKbutton'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(2)
WebUI.takeScreenshot()















